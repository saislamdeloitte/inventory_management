from simple_salesforce import Salesforce
import requests

from utils.general import get_setting


# def open_salesforce_connexion():
#     session = requests.Session()
#     sf = Salesforce(username=get_setting("CRED", "pphis"),
#                     password=get_setting("CRED", "password"),
#                     domain="test",
#                     security_token="voJClBM8xH3vumczZFg2fKAg", session=session)
#     return sf

def open_salesforce_connexion(env_name):
    session = requests.Session()
    if(env_name=="SIT"):
        sf = Salesforce(username=get_setting("CRED", "pphis_sit"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="ZugppvaxAevDd792jFomJ5a6n", session=session)
    elif(env_name=="UAT"):
        sf = Salesforce(username=get_setting("CRED", "pphis_uat"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="iaZpQgqPnOkDQf6CJPO2V0wNz", session=session)
    elif(env_name=="PRODSUPPQA"):
        sf = Salesforce(username=get_setting("CRED", "pphis_prodsuppqa"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="HkLvH7TSqfQY6q6PmaIE7RCC", session=session)
    return sf

#uat= iaZpQgqPnOkDQf6CJPO2V0wNz
def query_object_data(fields: object, obj: object, parameters: object, sf: object) -> object:
    # f = str(fields).strip("[]")  # Format the list of fields needed in the query from array to string
    query = "select " + fields + " from " + obj + " where " + parameters + ""
    #print(query)
    return sf.query(query)["records"][0]


def current_distribution_details(supp_dis_name,env_name):
    s = open_salesforce_connexion(env_name)
    #select HC_Measures_Tally__c,HC_Lot_Number__c,HC_Supply_Distribution__c,HC_Remaining_Measures__c,HC_Remaining_Quantity__c from HC_Supply_Container__c where Name='Moderna COVID-19 Vaccine - M888 (2021-03-19 14:59:24)'
    fields = 'HC_Measures_Tally__c,HC_Lot_Number__c,HC_Supply_Distribution__c,HC_Remaining_Measures__c,' \
             'HC_Remaining_Quantity__c'
    obj = "HC_Supply_Container__c"
    param = "Name = \'" + str(supp_dis_name) + "\'"
    result = query_object_data(fields, obj, param, s)
    #print("Remaining quantity: " + supp_dis_name + " | " + str(result["HC_Remaining_Quantity__c"]))
    return result["HC_Remaining_Quantity__c"]

def adjustment_distribution_details(supp_dis_name,env_name):
    s = open_salesforce_connexion(env_name)
    fields = 'HC_Measures_Tally__c,HC_Lot_Number__c,HC_Supply_Distribution__c,HC_Remaining_Measures__c,' \
             'HC_Remaining_Quantity__c'
    obj = "HC_Supply_Container__c"
    param = "Name = \'" + str(supp_dis_name) + "\'"
    result = query_object_data(fields, obj, param, s)
    #print("Remaining doses: " + supp_dis_name + " | " + str(result["HC_Remaining_Measures__c"]))
    return result["HC_Remaining_Measures__c"]

def get_supply_transaction_name(user_id,transaction_from_location,transaction_status,env_name):
    s = open_salesforce_connexion(env_name)
    fields = 'Name'
    obj = "HC_Supply_Transaction__c"
    param = "OwnerId = \'" + str(user_id) + "\'" "And HC_Supply_Location__c = \'" + str(transaction_from_location) + "\'" "And HC_Status__c = \'" + str(transaction_status) + "\'"
    result = query_object_data(fields, obj, param, s)
    return result["Name"]

def draft_transfer_supply_transaction_name(user_id,transaction_from_location,transaction_status,env_name):
    s = open_salesforce_connexion(env_name)
    fields = 'Name'
    obj = "HC_Supply_Transaction__c"
    param = "OwnerId = \'" + str(user_id) + "\'" "And HC_Supply_Location__c = \'" + str(transaction_from_location) + "\'" "And HC_Status__c = \'" + str(transaction_status) + "\'"
    result = query_object_data(fields, obj, param, s)
    return result["Name"]