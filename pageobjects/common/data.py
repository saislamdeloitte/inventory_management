import csv

header = ["Full Name", "First Name", "Last Name", "DOB", "PHN",'Received first dose?','PIR ID','POSTAL CODE','Reg No','Appointment No','Status','Errors','Immunization No']



with open('../../data.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)

    # write the header
    writer.writerow(header)
    data = "TEST PANOSHARPIE","TEST" ,"PANOSHARPIE" ,"Aug 01, 1988" ,"9879454958",'No','5588685','V0R2Y0','' ,'' ,'' ,'',''
    writer.writerow([data])
    data = 'JANTHREEFNAME JANTHREE','JANTHREEFNAME' ,'JANTHREE' ,'Feb 02, 1989' ,'9879881892','No','5588700','V0R2Y0','' ,'' ,'' ,'',''
    writer.writerow([data])
    data = 'BARBIE LABFLASTNAMETESTROBERT','BARBIE' ,'LABFLASTNAMETESTROBERT' ,'Jun 02, 1990' ,'9879455476','No','5588683','V0R2Y0','' ,'' ,'' ,'',''
    writer.writerow([data])
    data = 'TESTERE CGITEST','TESTERE' ,'CGITEST' ,'May 05, 1990' ,'9879453125','No','5588698','V0R2Y0','' ,'' ,'' ,'',''
    writer.writerow([data])
    data = 'ONE TESTINGTHELONGESLASTNAMEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE','ONE' ,'TESTINGTHELONGESLASTNAMEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE' ,'Sep 06, 1987' ,'9879886759','No','5589095','V0B1B0','' ,'' ,'' ,'',''
    writer.writerow([data])