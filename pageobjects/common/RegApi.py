from simple_salesforce import Salesforce
import requests

from utils.general import get_setting


def open_salesforce_connexion(env_name):
    session = requests.Session()
    if(env_name=="SIT"):
        sf = Salesforce(username=get_setting("CRED", "clinician_sit"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="dXAs4MHLUcT7BqOEILAHff7bg", session=session)
    elif(env_name=="UAT"):
        sf = Salesforce(username=get_setting("CRED", "clinician_uat"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="lS5fzObPrTp6VupvZ7gjmpSQ", session=session)
    elif(env_name=="PRODSUPPQA"):
        sf = Salesforce(username=get_setting("CRED", "clinician_prodsuppqa"),
                        password=get_setting("CRED", "password"),
                        domain="test",
                        security_token="Y8PVqhLOvUbgfOo6GNrfMYapF", session=session)
    return sf


def query_object_data(fields: object, obj: object, parameters: object, sf: object) -> object:
    # f = str(fields).strip("[]")  # Format the list of fields needed in the query from array to string
    query = "select " + fields + " from " + obj + " where " + parameters + ""
    #print(query)
    return sf.query(query)["records"][0]


def get_booking_url(reg_no,env_name):
    s = open_salesforce_connexion(env_name)
    fields = 'BCH_Unique_Link__c'
    obj = "Account"
    param = "BCH_Registration_Confirmation_Number__c = \'" + str(reg_no) + "\'"
    result = query_object_data(fields, obj, param, s)
    print("For reg no: " + reg_no + " | " + result["BCH_Unique_Link__c"])
    return result["BCH_Unique_Link__c"]


def get_booking_no(reg_no,env_name):
    s = open_salesforce_connexion(env_name)
    fields = 'BCH_Appointment_Confirmation_Number__c'
    obj = "DDH__HC_Session__c"
    param = "DDH__HC_Patient__c IN (Select Account.ID from Account where BCH_Registration_Confirmation_Number__c = \'" + str(
        reg_no) + "\')"
    result = query_object_data(fields, obj, param, s)
    print("For reg no: " + reg_no + " | " + result["BCH_Appointment_Confirmation_Number__c"])
    return result['BCH_Appointment_Confirmation_Number__c']


def get_citizen_record_url(reg_no,env_name):
    s = open_salesforce_connexion(env_name)
    # Select Id from Account where BCH_Registration_Confirmation_Number__c = 'R1EADNS35'
    fields = 'Id'
    obj = "Account"
    param = "BCH_Registration_Confirmation_Number__c = \'" + str(reg_no) + "\'"
    result = query_object_data(fields, obj, param, s)
    print("For reg no: " + reg_no + " | " + result["Id"])
    if(env_name=="SIT"):
        url = get_setting("URL", "sf_org_sit") + "/lightning/r/Account/" + result['Id'] + "/view"
    elif(env_name=="UAT"):
        url = get_setting("URL", "sf_org_uat") + "/lightning/r/Account/" + result['Id'] + "/view"
    elif(env_name=="PRODSUPPQA"):
        url = get_setting("URL", "sf_org_prodsuppqa") + "/lightning/r/Account/" + result['Id'] + "/view"
    return url
