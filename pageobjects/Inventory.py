import webbrowser
from datetime import datetime
from turtle import clear
import warnings
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from pandas.core.common import SettingWithCopyWarning
from .common import InventoryApi
from .common import inventory_transfer_excel
from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from .common import Testrail_Binding
from utils.general import *
import time


class Inventory(BASEPAGE):
    # supply_transaction_name_new=""
    locator_dictionary = {
        "user_name": (By.ID, 'username'),
        "password": (By.ID, 'password'),
        "login_btn": (By.ID, 'Login'),
        'supply_locations_tab': (By.XPATH, '//a[@title="Supply Locations"]'),
        "supply_location_1": (By.LINK_TEXT, 'Automation Supply Location_1'),
        "close_supply_location_1": (By.XPATH, '//button[@title="Close Automation Supply Location_1"]'),
        "close_supply_location_2": (By.XPATH, '//button[@title="Close Automation Supply Location_2"]'),
        "supply_location_2": (By.LINK_TEXT, 'Automation Supply Location_2'),
        "supply_container_name_link": (
            By.XPATH, '//td[@data-label="Supply Container Name"]//lightning-formatted-url'),
        "supply_container_name_link_2": (
            By.XPATH, '(//td[@data-label="Supply Container Name"]//div[@class="slds-hyphenate"])[2]'),
        "supply_container_name": (
            By.XPATH,
            '//div[@class="slds-grid slds-size_1-of-1 label-inline"]//following-sibling::div//span[@class="test-id__field-value slds-form-element__static slds-grow word-break-ie11"]//lightning-formatted-text'),
        "supply_location_ctic": (
            By.XPATH,
            '//ul[@class="tabBarItems slds-tabs--default__nav"]//span[text()="Automation Supply Location_1"]'),
        "related_items": (By.XPATH, '//a[@data-label="Related Items"]'),
        "transactions": (By.XPATH, '//a[@data-label="Transactions"]'),
        "down_arrow": (By.XPATH, '//button//span[text()="Show actions"]'),
        "transfer": (By.XPATH, '//a/span[text()="Transfer"]'),
        "draft_transfer_supply_name": (By.XPATH, '(//button//span[text()="Show actions"])[2]'),
        "draft_transfer_transaction_downwarrow": (By.XPATH,
                                                  '//a[@title="supplyItemName"][text()="Pfizer mRNA BNT162b2 - EL0203"]//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::td//following-sibling::td//following-sibling::td//following-sibling::td//button'),
        "draft": (By.XPATH, '//button[text()="Save as draft"]'),
        "guiding_text_doses_quantity": (By.XPATH, '//div[text()="Please fill in either the quantity or doses field."]'),
        "dose_conversion_factor": (By.XPATH, '//input[@name="HC_Product_Measure__c"]'),
        "doses_value": (By.XPATH, '(//input[@type="counter"])[2]'),
        "quantity_value": (By.XPATH, '(//input[@type="counter"])[1]'),
        "adjustment": (By.XPATH, '//a/span[text()="Adjustment"]'),
        "adjustment_remaining_doses": (By.XPATH, '//input[@name="HC_Remaining_Measures__c"]'),
        "wastage": (By.XPATH, '//a/span[text()="Wastage"]'),
        "quantity_doses": (
            By.XPATH, '//lightning-input//label[text()="Quantity"]//following-sibling::div/input[@class="slds-input"]'),
        "doses_check":(By.XPATH,'//lightning-input//label[text()="Doses"]//following-sibling::div/input[@class="slds-input"]'),
        "adjustment_doses": (By.XPATH, '//label[text()="Doses"]/following-sibling::div//input'),
        "reason_for_adjustment": (By.XPATH, '//input[@name="BCH_Reason_for_Adjustment__c"]'),
        "reason_for_adjustment_options": (
            By.XPATH, '//lightning-base-combobox-item[@data-value="Administered Vaccine"]'),
        "reason_for_wastage": (By.XPATH, '//input[@name="BCH_Reason_for_Wastage__c"]'),
        "reason_for_wastage_options": (By.XPATH, '//lightning-base-combobox-item[@data-value="Expired Product"]'),
        "adjustment_button": (By.XPATH, '(//button[text()="Adjustment"])[2]'),
        "wastage_button": (By.XPATH, '//button[text()="Wastage"]'),
        "transfer_supply_location": (By.XPATH, '//input[@placeholder="Search Supply Locations..."]'),
        "transfer_supply_location_UVIC": (
        By.XPATH, '//lightning-base-combobox-formatted-text[@title="Automation Supply Location_2"]'),
        "transfer_supply_location_1": (
            By.XPATH, '//lightning-base-combobox-formatted-text[@title="Automation Supply Location_1"]'),
        "supply_distribution_to": (By.XPATH, '// input[ @ placeholder = "Select an Option"]'),
        "supply_distribution_to_same_SIT": (By.XPATH, '//span[@title="None - SDST-0000000133"]'),
        "supply_distribution_to_same_UAT": (By.XPATH, '//span[@title="None - SDST-0000000242"]'),
        "supply_distribution_to_same_PRODSUPPQA": (By.XPATH, '//span[@title="None - SDST-0000000357"]'),
        "transfer_button": (By.XPATH, '//section[@role="dialog"]//button[text()="Transfer"]'),
        "close_button": (By.XPATH, '//section[@role="dialog"]//button[text()="Close"]'),
        "confirm_button_down_arrow": (By.XPATH,
                                      '//a[@title="transactionToName"]//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button'),
        "confirm_button": (By.XPATH, '//div[@role="menu"]//a/span[text()="Confirm"]'),
        "distribution_to": (By.XPATH, '//input[@placeholder="Select Supply Distributor"]'),
        "distribution_to_name_SIT": (By.XPATH, '//div//span[@title="None - SDST-0000000132"]'),
        "distribution_to_name_UAT": (By.XPATH, '//div//span[@title="None - SDST-0000000241"]'),
        "distribution_to_name_PRODSUPPQA": (By.XPATH, '//div//span[@title="None - SDST-0000000356"]'),
        "confirm_transaction": (By.XPATH, '//button[text()="Confirm Transaction"]'),
        "supply_distribution_2": (By.XPATH,
                                  '//td//a[text()="Moderna COVID-19 Vaccine - M888 (2021-05-31 21:59:56)"]/parent::lightning-formatted-url/parent::div/parent::span/parent::lightning-primitive-cell-factory/parent::td//preceding-sibling::th//lightning-base-formatted-text'),
        "confirm_toast": (By.XPATH, '//span[text()="You have successfully Confirmed the Transaction"]'),
        "wastage_toast": (By.XPATH, '//span[text()="The wastage of the specified quantity was performed."]'),
        "adjustment_toast": (By.XPATH, '//span[text()="The adjustment of the specified quantity was performed."]')
        # "existing_quantity": (By.XPATH, '//td[@data-label="Remaining Quantity"]//lightning-base-formatted-text'),
        # "rows":(By.XPATH,'//table[@role="grid"]//tr'),
        # "transferred_location": (By.XPATH, '//table//tr//td//a[text()="CTIC Coquitlam"]')
    }
    # //div[@class="slds-form-element slds-hint-parent test-id__output-root slds-form-element_edit slds-form-element_readonly slds-form-element_horizontal"]//span[text()="Supply Container Name"]//following-sibling::div//slot[@name="outputField"]
    constants = {
        "pphis": '/lightning/o/HC_Supply_Location__c/list'
    }

    def go_to(self, link):
        #print(link)
        if (link == "pphis_sit"):
            base_url = get_setting("URL", "sf_org_sit")
            supply_console_url = self.constants["pphis"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "pphis_uat"):
            base_url = get_setting("URL", "sf_org_uat")
            supply_console_url = self.constants["pphis"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "pphis_prodsuppqa"):
            base_url = get_setting("URL", "sf_org_prodsuppqa")
            supply_console_url = self.constants["pphis"]
            self.browser.get(base_url + supply_console_url)
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
        except:
            print("Exception: The user is not navigated to Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["login_btn"])
        assert var is not None, "The user is not navigated to Login screen."

    def login_into_website(self, user_name, password, home_screen):
        self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)
        self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
        # WebDriverWait(self.browser, 10).until(
        # EC.visibility_of_element_located(self.locator_dictionary["supply_location"]))
        # base_url = get_setting("URL", "sf_org")
        # self.browser.get(base_url + self.constants["pphis"])

    def transfer_doses(self, env_name):
        pass_result = 0
        #print(env_name)
        for inc in range(0, 1):
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            #self.browser.refresh()
            try:
                self.browser.implicitly_wait(3)
                WebDriverWait(self.browser, 3).until(
                    EC.visibility_of_element_located(self.locator_dictionary["close_supply_location_2"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["close_supply_location_2"]))
            except:
                assert "Carry on"
            try:
                self.browser.implicitly_wait(3)
                WebDriverWait(self.browser, 3).until(
                    EC.visibility_of_element_located(self.locator_dictionary["close_supply_location_1"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["close_supply_location_1"]))
            except:
                assert "carry on"
            #print(df['Iteration_No'][inc])
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            df = pd.read_csv('inventory.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            df['From Clinic'][inc] = "Automation Supply Location_1"
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            # existing_quantity = float(self.find_element(self.locator_dictionary["existing_quantity"]).text)
            # print(existing_quantity)
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_quantity = InventoryApi.current_distribution_details(supp_dis_name, env_name)
            #print(existing_quantity)
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["transfer"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
            #print(df['Amount of doses transferred'][inc])
            df['Quantity before transfer'][inc] = existing_quantity
            if quantity_doses == "0":
                print("entering quantity doses")
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
            transfer_supply_location = self.get_attribute(
                self.find_element(self.locator_dictionary["transfer_supply_location"]), "value")
            if transfer_supply_location == "":
                self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]),
                                          "Automation Supply Location_2")
                self.click_element(self.find_element(self.locator_dictionary["transfer_supply_location_UVIC"]))
                df['Amount of doses transferred'][inc] = float(
                    self.get_attribute(self.find_element(self.locator_dictionary["doses_value"]), "value"))
            # doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
            # doses_check = WebDriverWait(self.browser, 30).until(
            #     EC.visibility_of_element_located(self.locator_dictionary["doses_check"]), "value")
            # doses_check.clear()
            # self.send_text_to_element(self.find_element(self.locator_dictionary["doses_check"]), "NaN")
            doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
            #print("doses is:")
            #print(doses_check)
            #print("quantity_doses:")
            #print(quantity_doses)
            if quantity_doses == "0" or doses_check=="NaN":
                #print("quantity doses was zer0 or NaN")
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            else:
                #print("quantity doses was not zer0 or NaN")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            self.click_element(self.find_element(self.locator_dictionary["close_button"]))
            # self.browser.refresh()
            df['To Clinic'][inc] = "Automation Supply Location_2"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_quantity = InventoryApi.current_distribution_details(supp_dis_name, env_name)
            #print(remaining_quantity)
            df['Quantity after transfer'][inc] = remaining_quantity
            # remaining_quantity = float(self.find_element(self.locator_dictionary["existing_quantity"]).text)
            # print(remaining_quantity)
            if remaining_quantity == existing_quantity - 1:
                print("Remaining doses quantity calculations after transfer between two clinics are correct")
                # df['Flow Result'][inc]="Pass"
            else:
                print("Remaining doses quantity calculations after transfer between two clinics are wrong")
                # df['Flow Result'][inc] = "Fail"
            if (env_name == "SIT"):
                user_id = '0054m000000Z9OiAAK'
                transaction_from_location = 'a2c4m0000000wy3AAA'
                transaction_status = 'Shipped'
            elif (env_name == "UAT"):
                user_id = '0054m000000ZpGgAAK'
                transaction_from_location = 'a2c4m0000000wz1AAA'
                transaction_status = 'Shipped'
            elif (env_name == "PRODSUPPQA"):
                user_id = '0054m000000ZposAAC'
                transaction_from_location = 'a2c4m0000000xLqAAI'
                transaction_status = 'Shipped'
            supply_transaction_name = InventoryApi.get_supply_transaction_name(user_id, transaction_from_location,
                                                                               transaction_status, env_name)
            str_only = supply_transaction_name[0:5]
            transaction_number = (supply_transaction_name[5:])
            last_4_transaction_number = int(transaction_number)
            #print(last_4_transaction_number)
            last_4_transaction_number = last_4_transaction_number + 1
            last_4_transaction_number = str(last_4_transaction_number)
            if(env_name=="PRODSUPPQA"):
                supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
            elif (env_name == "UAT"):
                supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
            else:
                supply_transaction_name_new = str_only + "000000" + last_4_transaction_number
            #print(supply_transaction_name_new)
            self.click_element(self.find_element(self.locator_dictionary["close_supply_location_1"]))
            # self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]), "UVIC")
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["supply_location_2"]))
            # self.click_element(self.find_element(self.locator_dictionary["supply_location_2"]))
            self.click_element(self.find_element(self.locator_dictionary["transactions"]))
            down_arrow_button = "//a[@title='transactionToName' and text()='" + supply_transaction_name_new + "']//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button"
            #print(down_arrow_button)
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located((By.XPATH, down_arrow_button)))
            self.browser.execute_script("arguments[0].click();", e)
            self.click_element(self.find_element(self.locator_dictionary["confirm_button"]))
            self.click_element(self.find_element(self.locator_dictionary["distribution_to"]))
            if env_name == "SIT":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_SIT"]))
            elif env_name == "UAT":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_UAT"]))
            elif env_name == "PRODSUPPQA":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_PRODSUPPQA"]))
            self.click_element(self.find_element(self.locator_dictionary["confirm_transaction"]))
            # self.browser.refresh()
            try:
                # identify element
                l = self.find_element(self.locator_dictionary["confirm_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["close_supply_location_2"]))
            # self.click_element(self.find_element(self.locator_dictionary["close_supply_location_2"]))
            df.to_csv('inventory.csv', index=False)
            self.browser.refresh()
            total_rows = len(df.index)
            #print("total row=")
            #print(total_rows)
        # for inc in range(0, total_rows):
        #     if(df['Flow Result'][inc] == "Pass"):
        #         pass_result=pass_result+1
        #         #print(pass_result)
        # if pass_result==total_rows:
        #     Testrail_Binding.testrail_success()
        # else:
        #     Testrail_Binding.testrail_fail()

    def transfer_doses_bulk_transfer(self):
        for inc in range(0, 50):
            # identify element
            try:
                self.find_element(self.locator_dictionary["close_supply_location_2"])
                # if(self.find_element(self.locator_dictionary["close_supply_location_2"]).isdisplayed()):
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["close_supply_location_2"]))
            except:
                assert "Carry on"
            df = pd.read_csv('inventory_2.csv')
            # df1=pd.DataFrame()
            # df1 = df1.append(inc, ignore_index=True)
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            #print(df['Iteration_No'][inc])
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            df['From Clinic'][inc] = "CTIC Coquitlam"
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            # existing_quantity = float(self.find_element(self.locator_dictionary["existing_quantity"]).text)
            # print(existing_quantity)
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_quantity = InventoryApi.current_distribution_details(supp_dis_name)
            #print(existing_quantity)
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["transfer"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
            #print(df['Amount of doses transferred'][inc])
            df['Quantity before transfer'][inc] = existing_quantity
            if quantity_doses == "0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
            transfer_supply_location = self.get_attribute(
                self.find_element(self.locator_dictionary["transfer_supply_location"]), "value")
            if transfer_supply_location == "":
                self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]),
                                          "UVIC")
                self.click_element(self.find_element(self.locator_dictionary["transfer_supply_location_UVIC"]))
                df['Amount of doses transferred'][inc] = float(
                    self.get_attribute(self.find_element(self.locator_dictionary["doses_value"]), "value"))
            if quantity_doses == "0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            else:
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            self.click_element(self.find_element(self.locator_dictionary["close_button"]))
            # self.browser.refresh()
            df['To Clinic'][inc] = "UVIC"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_quantity = InventoryApi.current_distribution_details(supp_dis_name)
            #print(remaining_quantity)
            df['Quantity after transfer'][inc] = remaining_quantity
            if remaining_quantity == existing_quantity - 1:
                print("Remaining doses quantity calculations are correct")
            else:
                print("Remaining doses quantity calculations are wrong")
            user_id = '0054m000000ZXRlAAO'
            transaction_from_location = 'a2c4m0000000XsKAAU'
            transaction_status = 'Shipped'
            supply_transaction_name = InventoryApi.get_supply_transaction_name(user_id, transaction_from_location,
                                                                               transaction_status)
            # print(supply_transaction_name)
            str_only = supply_transaction_name[0:5]
            transaction_number = (supply_transaction_name[5:])
            last_4_transaction_number = int(transaction_number)
            last_4_transaction_number = last_4_transaction_number + 1
            last_4_transaction_number = str(last_4_transaction_number)
            supply_transaction_name_new = str_only + "000000" + last_4_transaction_number
            self.click_element(self.find_element(self.locator_dictionary["close_supply_location_1"]))
            # self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]), "UVIC")
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["supply_location_2"]))
            self.click_element(self.find_element(self.locator_dictionary["transactions"]))
            down_arrow_button = "//a[@title='transactionToName' and text()='" + supply_transaction_name_new + "']//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button"

            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located((By.XPATH, down_arrow_button)))
            self.browser.execute_script("arguments[0].click();", e)
            self.click_element(self.find_element(self.locator_dictionary["confirm_button"]))
            self.click_element(self.find_element(self.locator_dictionary["distribution_to"]))
            self.click_element(self.find_element(self.locator_dictionary["distribution_to_name"]))
            self.click_element(self.find_element(self.locator_dictionary["confirm_transaction"]))
            try:
                # identify element
                l = self.find_element(self.locator_dictionary["confirm_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            # self.browser.refresh()
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["close_supply_location_2"]))
            # self.click_element(self.find_element(self.locator_dictionary["close_supply_location_2"]))
            df.to_csv('inventory_2.csv', index=False)
            self.browser.refresh()

    def transfer_doses_same_clinic(self,env_name):
        self.click_element(self.find_element(self.locator_dictionary["close_supply_location_2"]))
        self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
        self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
        self.click_element(self.find_element(self.locator_dictionary["related_items"]))
        supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
        # supp_dis_desc = self.find_element(self.locator_dictionary["supply_distribution_2"]).text
        self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
        existing_quantity = InventoryApi.current_distribution_details(supp_dis_name,env_name)
        #print(existing_quantity)
        self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["down_arrow"]))
        self.click_element(self.find_element(self.locator_dictionary["transfer"]))
        try:
            self.find_element(self.locator_dictionary["guiding_text_doses_quantity"])
        except:
            assert "The text 'Please fill in either the quantity or doses field.' is not found"
        quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
        if quantity_doses == "0":
            quantity_doses = WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
            quantity_doses.clear()
            self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), 1)
        dose_conversion_factor = float(self.get_attribute(
            self.find_element(self.locator_dictionary["dose_conversion_factor"]), "value"))
        doses_value = float(self.get_attribute(self.find_element(self.locator_dictionary["doses_value"]), "value"))
        quantity_value = float(
            self.get_attribute(self.find_element(self.locator_dictionary["quantity_value"]), "value"))
        if doses_value == quantity_value * dose_conversion_factor:
            assert "Doses value calculation is true"
        else:
            assert "Doses value calculation is wrong"
        transfer_supply_location = self.get_attribute(
            self.find_element(self.locator_dictionary["transfer_supply_location"]), "value")
        if transfer_supply_location == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]),
                                      "Automation Supply Location_1")
            self.click_element(self.find_element(self.locator_dictionary["transfer_supply_location_1"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["supply_distribution_to"]))
            # self.click_element(self.find_element(self.locator_dictionary["supply_distribution_to"]))
            if env_name == "SIT":
                self.click_element(self.find_element(self.locator_dictionary["supply_distribution_to_same_SIT"]))
            elif env_name == "UAT":
                self.click_element(self.find_element(self.locator_dictionary["supply_distribution_to_same_UAT"]))
            elif env_name == "PRODSUPPQA":
                self.click_element(self.find_element(self.locator_dictionary["supply_distribution_to_same_PRODSUPPQA"]))
            #self.click_element(self.find_element(self.locator_dictionary["supply_distribution_to_name"]))
            # doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
            # doses_check = WebDriverWait(self.browser, 30).until(
            #     EC.visibility_of_element_located(self.locator_dictionary["doses_check"]), "value")
            # doses_check.clear()
            # self.send_text_to_element(self.find_element(self.locator_dictionary["doses_check"]), "NaN")
            doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
            # print("doses is:")
            # print(doses_check)
            # print("quantity_doses:")
            # print(quantity_doses)
            if quantity_doses == "0" or doses_check == "NaN":
                # print("quantity doses was zer0 or NaN")
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            else:
                # print("quantity doses was not zer0 or NaN")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            # self.browser.execute_script("arguments[0].scrollIntoView();",
            #                             self.find_element(self.locator_dictionary["transfer_button"]))
            # self.browser.execute_script("arguments[0].click();",
            #                             self.find_element(self.locator_dictionary["transfer_button"]))
        self.click_element(self.find_element(self.locator_dictionary["close_button"]))
        self.browser.refresh()
        WebDriverWait(self.browser, 30).until(
            EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
        self.click_element(self.find_element(self.locator_dictionary["related_items"]))
        supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
        self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
        remaining_quantity = InventoryApi.current_distribution_details(supp_dis_name,env_name)
        #print(remaining_quantity)
        if remaining_quantity == existing_quantity - 1:
            print("Remaining doses quantity calculations after transfer within the same clinic are correct")
        else:
            print("Remaining doses quantity calculations after transfer within the same clinic are wrong")

    def container_adjustment(self,env_name):
        for inc in range(0, 1):
            df = pd.read_csv('inventory_adjustment.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            #print(df['Iteration_No'][inc])
            df['Clinic'][inc] = "Automation Supply Location_1"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            # supp_dis_desc = self.find_element(self.locator_dictionary["supply_distribution_2"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_doses = InventoryApi.adjustment_distribution_details(supp_dis_name,env_name)
            df['Doses before adjustment'][inc] = existing_doses
            #print(existing_doses)
            df['Doses before adjustment'][inc] = existing_doses
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["adjustment"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["adjustment_doses"]), "value")
            if quantity_doses == "" or quantity_doses=="0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["adjustment_doses"]), "10")
            adjustment_remaining_doses = (
                self.get_attribute(self.find_element(self.locator_dictionary["adjustment_remaining_doses"]), "value"))
            df['Amount of doses adjusted'][inc] = 10
            #print(existing_doses)
            if(adjustment_remaining_doses.find(',')!=-1):
                #print("comma found")
                adjustment_remaining_doses=int(adjustment_remaining_doses.replace(',',''))
            else:
                #print("comma not found")
                adjustment_remaining_doses = int(adjustment_remaining_doses)
            #print(adjustment_remaining_doses)
            if adjustment_remaining_doses == existing_doses + 10:
                print("Remaining doses in the container adjustment popup calculation is correct")
            else:
                #print(adjustment_remaining_doses)
                print("Remaining doses in the container adjustment popup calculation is wrong")
            self.click_element(self.find_element(self.locator_dictionary["reason_for_adjustment"]))
            self.click_element(self.find_element(self.locator_dictionary["reason_for_adjustment_options"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["adjustment_button"]))
            # self.browser.refresh()
            try:
                # identify element
                l = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_toast"]))
                # l = self.find_element(self.locator_dictionary["wastage_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_doses = InventoryApi.adjustment_distribution_details(supp_dis_name,env_name)
            #print(remaining_doses)
            df['Doses after adjustment'][inc] = remaining_doses
            if remaining_doses == existing_doses + 10:
                print("Remaining doses calculations after adjustment are correct")
                # df['Flow Result'][inc] = "Pass"
            else:
                print("Remaining doses calculations after adjustment are wrong")
                # df['Flow Result'][inc] = "Fail"
            df.to_csv('inventory_adjustment.csv', index=False)
            self.browser.refresh()

    def container_adjustment_bulk(self):
        for inc in range(0, 10):
            df = pd.read_csv('inventory_adjustment_2.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            print(df['Iteration_No'][inc])
            df['Clinic'][inc] = "CTIC Coquitlam"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            # supp_dis_desc = self.find_element(self.locator_dictionary["supply_distribution_2"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_doses = InventoryApi.adjustment_distribution_details(supp_dis_name)
            df['Doses before adjustment'][inc] = existing_doses
            print(existing_doses)
            df['Doses before adjustment'][inc] = existing_doses
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["adjustment"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["adjustment_doses"]), "value")
            if quantity_doses == "":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["adjustment_doses"]), "10")
            adjustment_remaining_doses = (
                self.get_attribute(self.find_element(self.locator_dictionary["adjustment_remaining_doses"]), "value"))
            df['Amount of doses adjusted'][inc] = 10
            if adjustment_remaining_doses == existing_doses + 10:
                print("Remaining doses in the container adjustment popup calculation is correct")
            else:
                print(adjustment_remaining_doses)
                print("Remaining doses in the container adjustment popup calculation is wrong")
            self.click_element(self.find_element(self.locator_dictionary["reason_for_adjustment"]))
            self.click_element(self.find_element(self.locator_dictionary["reason_for_adjustment_options"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["adjustment_button"]))
            # self.browser.refresh()
            try:
                # identify element
                l = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_toast"]))
                # l = self.find_element(self.locator_dictionary["wastage_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_doses = InventoryApi.adjustment_distribution_details(supp_dis_name)
            print(remaining_doses)
            df['Doses after adjustment'][inc] = remaining_doses
            if remaining_doses == existing_doses + 10:
                print("Remaining doses calculations are correct")
                # df['Flow Result'][inc] = "Pass"
            else:
                print("Remaining doses calculations are wrong")
                # df['Flow Result'][inc] = "Fail"
            df.to_csv('inventory_adjustment_2.csv', index=False)
            self.browser.refresh()

    def container_wastage(self,env_name):
        for inc in range(0, 1):
            df = pd.read_csv('inventory_wastage.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            #print(df['Iteration_No'][inc])
            df['Clinic'][inc] = "Automation Supply Location_1"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            # supp_dis_desc = self.find_element(self.locator_dictionary["supply_distribution_2"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_doses = InventoryApi.adjustment_distribution_details(supp_dis_name,env_name)
            df['Doses before wastage'][inc] = existing_doses
            #print(existing_doses)
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["wastage"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["adjustment_doses"]), "value")
            if quantity_doses == "" or quantity_doses=="0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["adjustment_doses"]), "2")
            adjustment_remaining_doses = self.get_attribute(
                self.find_element(self.locator_dictionary["adjustment_remaining_doses"]), "value")
            if(adjustment_remaining_doses.find(',')!=-1):
                #print("comma found")
                adjustment_remaining_doses=int(adjustment_remaining_doses.replace(',',''))
            else:
                #print("comma not found")
                adjustment_remaining_doses = int(adjustment_remaining_doses)
            if adjustment_remaining_doses == existing_doses - 2:
                print("Remaining doses in the container wastage popup calculation is correct")
            else:
                #print(adjustment_remaining_doses)
                print("Remaining doses in the container wastage popup calculation is wrong")
            df['Amount of doses wasted'][inc] = 2
            self.click_element(self.find_element(self.locator_dictionary["reason_for_wastage"]))
            self.click_element(self.find_element(self.locator_dictionary["reason_for_wastage_options"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["wastage_button"]))
            # self.click_element(self.find_element(self.locator_dictionary["wastage_button"]))
            try:
                # identify element
                l = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["wastage_toast"]))
                # l = self.find_element(self.locator_dictionary["wastage_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            # self.browser.refresh()
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_doses = InventoryApi.adjustment_distribution_details(supp_dis_name,env_name)
            #print(remaining_doses)
            df['Doses after wastage'][inc] = remaining_doses
            if remaining_doses == existing_doses - 2:
                print("Remaining doses calculations after wastage are correct")
            else:
                print("Remaining doses calculations after wastage are wrong")
            df.to_csv('inventory_wastage.csv', index=False)
            self.browser.refresh()
            # self.click_element(self.find_element(self.locator_dictionary["login_btn"]))

    def container_wastage_bulk(self):
        for inc in range(0, 50):
            df = pd.read_csv('inventory_wastage_2.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            print(df['Iteration_No'][inc])
            df['Clinic'][inc] = "CTIC Coquitlam"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            # supp_dis_desc = self.find_element(self.locator_dictionary["supply_distribution_2"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_doses = InventoryApi.adjustment_distribution_details(supp_dis_name)
            df['Doses before wastage'][inc] = existing_doses
            print(existing_doses)
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["wastage"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["adjustment_doses"]), "value")
            if quantity_doses == "":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["adjustment_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["adjustment_doses"]), "2")
            adjustment_remaining_doses = self.get_attribute(
                self.find_element(self.locator_dictionary["adjustment_remaining_doses"]), "value")
            if adjustment_remaining_doses == existing_doses - 2:
                print("Remaining doses in the container wastage popup calculation is correct")
            else:
                print(adjustment_remaining_doses)
                print("Remaining doses in the container wastage popup calculation is wrong")
            df['Amount of doses wasted'][inc] = 2
            self.click_element(self.find_element(self.locator_dictionary["reason_for_wastage"]))
            self.click_element(self.find_element(self.locator_dictionary["reason_for_wastage_options"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["wastage_button"]))
            # self.click_element(self.find_element(self.locator_dictionary["wastage_button"]))
            try:
                # identify element
                l = self.find_element(self.locator_dictionary["wastage_toast"])
                s = l.text
                df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                df['Flow Result'][inc] = "Fail"
            # self.browser.refresh()
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_doses = InventoryApi.adjustment_distribution_details(supp_dis_name)
            print(remaining_doses)
            df['Doses after wastage'][inc] = remaining_doses
            if remaining_doses == existing_doses - 2:
                print("Remaining doses calculations are correct")
                # df['Flow Result'][inc] = "Pass"
            else:
                print("Remaining doses calculations are wrong")
                # df['Flow Result'][inc] = "Fail"
            df.to_csv('inventory_wastage_2.csv', index=False)
            self.browser.refresh()

    def draft_transfer(self,env_name):
        self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
        self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
        self.click_element(self.find_element(self.locator_dictionary["related_items"]))
        supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
        self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
        existing_quantity = InventoryApi.current_distribution_details(supp_dis_name,env_name)
        #print(existing_quantity)
        self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["draft_transfer_supply_name"]))
        self.click_element(self.find_element(self.locator_dictionary["transfer"]))
        quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
        if quantity_doses == "0":
            quantity_doses = WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
            quantity_doses.clear()
            self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
        transfer_supply_location = self.get_attribute(
            self.find_element(self.locator_dictionary["transfer_supply_location"]), "value")
        if transfer_supply_location == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]), "Automation Supply Location_2")
            self.click_element(self.find_element(self.locator_dictionary["transfer_supply_location_UVIC"]))
        # doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
        # doses_check = WebDriverWait(self.browser, 30).until(
        #     EC.visibility_of_element_located(self.locator_dictionary["doses_check"]), "value")
        # doses_check.clear()
        # self.send_text_to_element(self.find_element(self.locator_dictionary["doses_check"]), "NaN")
        doses_check = self.get_attribute(self.find_element(self.locator_dictionary["doses_check"]), "value")
        quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
        # print("doses is:")
        # print(doses_check)
        # print("quantity_doses:")
        # print(quantity_doses)
        if quantity_doses == "0" or doses_check == "NaN":
            # print("quantity doses was zer0 or NaN")
            quantity_doses = WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
            quantity_doses.clear()
            self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
            self.browser.execute_script("arguments[0].scrollIntoView();",
                                        self.find_element(self.locator_dictionary["draft"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["draft"]))
        else:
            # print("quantity doses was not zer0 or NaN")
            self.browser.execute_script("arguments[0].scrollIntoView();",
                                        self.find_element(self.locator_dictionary["draft"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["draft"]))
        self.click_element(self.find_element(self.locator_dictionary["close_button"]))
        self.browser.refresh()
        WebDriverWait(self.browser, 30).until(
            EC.visibility_of_element_located(self.locator_dictionary["transactions"]))
        self.click_element(self.find_element(self.locator_dictionary["transactions"]))
        if (env_name == "SIT"):
            user_id = '0054m000000Z9OiAAK'
            transaction_from_location = 'a2c4m0000000wy3AAA'
            transaction_status = 'Draft'
        elif (env_name == "UAT"):
            user_id = '0054m000000ZpGgAAK'
            transaction_from_location = 'a2c4m0000000wz1AAA'
            transaction_status = 'Draft'
        elif (env_name == "PRODSUPPQA"):
            user_id = '0054m000000ZposAAC'
            transaction_from_location = 'a2c4m0000000xLqAAI'
            transaction_status = 'Draft'
        supply_transaction_name = InventoryApi.draft_transfer_supply_transaction_name(user_id, transaction_from_location,
                                                                           transaction_status, env_name)
        draft_transfer_transaction_downwarrow = "//a[@title='draftTransactionFromName' and text()='" + supply_transaction_name + "']//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button"
        #print(draft_transfer_transaction_downwarrow)
        e = WebDriverWait(self.browser, 20).until(
            EC.presence_of_element_located((By.XPATH, draft_transfer_transaction_downwarrow)))
        self.browser.execute_script("arguments[0].click();", e)
        #self.click_element(self.find_element(self.locator_dictionary["draft_transfer_transaction_downwarrow"]))
        self.click_element(self.find_element(self.locator_dictionary["transfer"]))
        self.browser.refresh()
        self.click_element(self.find_element(self.locator_dictionary["close_supply_location_1"]))
        self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
        self.click_element(self.find_element(self.locator_dictionary["supply_location_2"]))
        self.click_element(self.find_element(self.locator_dictionary["transactions"]))
        if (env_name == "SIT"):
            user_id = '0054m000000Z9OiAAK'
            transaction_from_location = 'a2c4m0000000wy3AAA'
            transaction_status = 'Shipped'
        elif (env_name == "UAT"):
            user_id = '0054m000000ZpGgAAK'
            transaction_from_location = 'a2c4m0000000wz1AAA'
            transaction_status = 'Shipped'
        elif (env_name == "PRODSUPPQA"):
            user_id = '0054m000000ZposAAC'
            transaction_from_location = 'a2c4m0000000xLqAAI'
            transaction_status = 'Shipped'
        supply_transaction_name = InventoryApi.get_supply_transaction_name(user_id, transaction_from_location,
                                                                           transaction_status, env_name)
        str_only = supply_transaction_name[0:5]
        transaction_number = (supply_transaction_name[5:])
        last_4_transaction_number = int(transaction_number)
        #print(last_4_transaction_number)
        last_4_transaction_number = last_4_transaction_number + 1
        last_4_transaction_number = str(last_4_transaction_number)
        if (env_name == "PRODSUPPQA"):
            supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
        elif (env_name == "UAT"):
            supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
        else:
            supply_transaction_name_new = str_only + "000000" + last_4_transaction_number
        #print(supply_transaction_name_new)
        #self.click_element(self.find_element(self.locator_dictionary["close_supply_location_1"]))
        # self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]), "UVIC")
        self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["supply_location_2"]))
        # self.click_element(self.find_element(self.locator_dictionary["supply_location_2"]))
        self.click_element(self.find_element(self.locator_dictionary["transactions"]))
        down_arrow_button = "//a[@title='transactionToName' and text()='" + supply_transaction_name_new + "']//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button"
        #print(down_arrow_button)
        e = WebDriverWait(self.browser, 20).until(
            EC.presence_of_element_located((By.XPATH, down_arrow_button)))
        self.browser.execute_script("arguments[0].click();", e)
        #self.click_element(self.find_element(self.locator_dictionary["confirm_button_down_arrow"]))
        self.click_element(self.find_element(self.locator_dictionary["confirm_button"]))
        self.click_element(self.find_element(self.locator_dictionary["distribution_to"]))
        if env_name == "SIT":
            self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_SIT"]))
        elif env_name == "UAT":
            self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_UAT"]))
        elif env_name == "PRODSUPPQA":
            self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_PRODSUPPQA"]))
        #self.click_element(self.find_element(self.locator_dictionary["distribution_to_name"]))
        self.click_element(self.find_element(self.locator_dictionary["confirm_transaction"]))
        self.browser.refresh()

    def transfer_doses_failed(self, env_name):
        pass_result = 0
        #print(env_name)
        for inc in range(0, 1):
            # try:
            #   self.find_element(self.locator_dictionary["close_supply_location_2"])
            #   self.browser.execute_script("arguments[0].click();",
            #   self.find_element(self.locator_dictionary["close_supply_location_2"]))
            # except:
            #   assert "Carry on"
            df = pd.read_csv('inventory.csv')
            warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
            df['Iteration_No'][inc] = int(inc + 1)
            #print(df['Iteration_No'][inc])
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.click_element(self.find_element(self.locator_dictionary["supply_location_1"]))
            df['From Clinic'][inc] = "Automation Supply Location_1"
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            # existing_quantity = float(self.find_element(self.locator_dictionary["existing_quantity"]).text)
            # print(existing_quantity)
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            existing_quantity = InventoryApi.current_distribution_details(supp_dis_name, env_name)
            #print(existing_quantity)
            self.click_element(self.find_element(self.locator_dictionary["supply_location_ctic"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["down_arrow"]))
            self.click_element(self.find_element(self.locator_dictionary["transfer"]))
            quantity_doses = self.get_attribute(self.find_element(self.locator_dictionary["quantity_doses"]), "value")
            #print(df['Amount of doses transferred'][inc])
            df['Quantity before transfer'][inc] = existing_quantity
            if quantity_doses == "0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
            transfer_supply_location = self.get_attribute(
                self.find_element(self.locator_dictionary["transfer_supply_location"]), "value")
            if transfer_supply_location == "":
                self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]),
                                          "Automation Supply Location_2")
                self.click_element(self.find_element(self.locator_dictionary["transfer_supply_location_UVIC"]))
                df['Amount of doses transferred'][inc] = float(
                    self.get_attribute(self.find_element(self.locator_dictionary["doses_value"]), "value"))
            if quantity_doses == "0":
                quantity_doses = WebDriverWait(self.browser, 30).until(
                    EC.visibility_of_element_located(self.locator_dictionary["quantity_doses"]))
                quantity_doses.clear()
                self.send_text_to_element(self.find_element(self.locator_dictionary["quantity_doses"]), "1")
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            else:
                self.browser.execute_script("arguments[0].scrollIntoView();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
                self.browser.execute_script("arguments[0].click();",
                                            self.find_element(self.locator_dictionary["transfer_button"]))
            self.click_element(self.find_element(self.locator_dictionary["close_button"]))
            # self.browser.refresh()
            df['To Clinic'][inc] = "Automation Supply Location_2"
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_items"]))
            self.click_element(self.find_element(self.locator_dictionary["related_items"]))
            supp_dis_name = self.find_element(self.locator_dictionary["supply_container_name_link"]).text
            self.click_element(self.find_element(self.locator_dictionary["supply_container_name_link"]))
            remaining_quantity = InventoryApi.current_distribution_details(supp_dis_name, env_name)
            #print(remaining_quantity)
            df['Quantity after transfer'][inc] = remaining_quantity
            # remaining_quantity = float(self.find_element(self.locator_dictionary["existing_quantity"]).text)
            # print(remaining_quantity)
            try:
                if remaining_quantity == existing_quantity:
                    print("Remaining doses quantity calculations are correct")
                    df['Flow Result'][inc] = "pass"
                else:
                    print("Remaining doses quantity calculations are wrong")
                    print("You will need to change either the agent on the supply item or the agent on the trade to make sure that they are matching")
                    df['Flow Result'][inc] = "Fail"
            except:
                assert "Pass"
            if (env_name == "SIT"):
                user_id = '0054m000000Z9OiAAK'
                transaction_from_location = 'a2c4m0000000wy3AAA'
                transaction_status = 'Shipped'
            elif (env_name == "UAT"):
                user_id = '0054m000000ZpGgAAK'
                transaction_from_location = 'a2c4m0000000wz1AAA'
                transaction_status = 'Shipped'
            elif (env_name == "PRODSUPPQA"):
                user_id = '0054m000000ZposAAC'
                transaction_from_location = 'a2c4m0000000xLqAAI'
                transaction_status = 'Shipped'
            supply_transaction_name = InventoryApi.get_supply_transaction_name(user_id, transaction_from_location,
                                                                               transaction_status, env_name)
            str_only = supply_transaction_name[0:5]
            transaction_number = (supply_transaction_name[5:])
            last_4_transaction_number = int(transaction_number)
            #print(last_4_transaction_number)
            last_4_transaction_number = last_4_transaction_number + 1
            last_4_transaction_number = str(last_4_transaction_number)
            if(env_name=="PRODSUPPQA"):
                supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
            elif (env_name == "UAT"):
                supply_transaction_name_new = str_only + "0000" + last_4_transaction_number
            else:
                supply_transaction_name_new = str_only + "000000" + last_4_transaction_number
            #print(supply_transaction_name_new)
            self.click_element(self.find_element(self.locator_dictionary["close_supply_location_1"]))
            # self.send_text_to_element(self.find_element(self.locator_dictionary["transfer_supply_location"]), "UVIC")
            self.click_element(self.find_element(self.locator_dictionary["supply_locations_tab"]))
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["supply_location_2"]))
            # self.click_element(self.find_element(self.locator_dictionary["supply_location_2"]))
            self.click_element(self.find_element(self.locator_dictionary["transactions"]))
            down_arrow_button = "//a[@title='transactionToName' and text()='" + supply_transaction_name_new + "']//parent::lightning-formatted-url//parent::div//parent::span/parent::lightning-primitive-cell-factory/parent::th//following-sibling::td//following-sibling::td//following-sibling::td//button"
            #print(down_arrow_button)
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located((By.XPATH, down_arrow_button)))
            self.browser.execute_script("arguments[0].click();", e)
            self.click_element(self.find_element(self.locator_dictionary["confirm_button"]))
            self.click_element(self.find_element(self.locator_dictionary["distribution_to"]))
            if env_name == "SIT":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_SIT"]))
            elif env_name == "UAT":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_UAT"]))
            elif env_name == "PRODSUPPQA":
                self.click_element(self.find_element(self.locator_dictionary["distribution_to_name_PRODSUPPQA"]))
            self.click_element(self.find_element(self.locator_dictionary["confirm_transaction"]))
            # self.browser.refresh()
            try:
                # identify element
                l = self.find_element(self.locator_dictionary["confirm_toast"])
                s = l.text
                #df['Flow Result'][inc] = "Pass"
            # NoSuchElementException thrown if not present
            except:
                assert "pass"
                #df['Flow Result'][inc] = "Fail"
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["close_supply_location_2"]))
            # self.click_element(self.find_element(self.locator_dictionary["close_supply_location_2"]))
            df.to_csv('inventory.csv', index=False)
            self.browser.refresh()
            total_rows = len(df.index)
            #print("total row=")
            #print(total_rows)
        # for inc in range(0, total_rows):
        #     if(df['Flow Result'][inc] == "Pass"):
        #         pass_result=pass_result+1
        #         #print(pass_result)
        # if pass_result==total_rows:
        #     Testrail_Binding.testrail_success()
        # else:
        #     Testrail_Binding.testrail_fail()
            raise Exception("You will need to change either the agent on the supply item or the agent on the trade to make sure that they are matching")
        #self.click_element(self.find_element(self.locator_dictionary["confirm_transaction"]))