Feature: Registration flows

  @reg_thru_portal_SIT
  Scenario: Citizen Portal Flow SIT
    Given user is on Citizen portal HOME page as SIT
    When the user Register and books an appointment using the portal in SIT.

  @reg_thru_portal_UAT
  Scenario: Citizen Portal Flow UAT
    Given user is on Citizen portal HOME page as UAT
    When the user Register and books an appointment using the portal in UAT.

  @reg_thru_portal_PRODSUPPQA
  Scenario: Citizen Portal Flow PRODSUPPQA
    Given user is on Citizen portal HOME page as PRODSUPPQA
    When the user Register and books an appointment using the portal in PRODSUPPQA.

  @reg_user_thru_call_center_SIT
  Scenario: Call Center Agent Flow SIT
    Given user is on call_center_agent_sit Login Page.
    When the user provide the call_center_agent_sit username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in SIT environment.
    Then the user Register and books an appointment using the call_center_agent in SIT.

  @reg_user_thru_call_center_UAT
  Scenario: Call Center Agent Flow UAT
    Given user is on call_center_agent_uat Login Page.
    When the user provide the call_center_agent_uat username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in UAT environment.
    Then the user Register and books an appointment using the call_center_agent in UAT.

  @reg_user_thru_call_center_PRODSUPPQA
  Scenario: Call Center Agent Flow PRODSUPPQA
    Given user is on call_center_agent_prodsuppqa Login Page.
    When the user provide the call_center_agent_prodsuppqa username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in PRODSUPPQA environment.
    Then the user Register and books an appointment using the call_center_agent in PRODSUPPQA.

  @reg_user_thru_clinician_SIT
  Scenario: Clinician Flow SIT
    Given user is on clinician_sit Login Page.
    When the user provide the clinician_sit username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in SIT environment.
    Then the user Register and books an appointment using the clinician in SIT.

  @reg_user_thru_clinician_UAT
  Scenario: Clinician Flow UAT
    Given user is on clinician_uat Login Page.
    When the user provide the clinician_uat username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in UAT environment.
    Then the user Register and books an appointment using the clinician in UAT.

  @reg_user_thru_clinician_PRODSUPPQA
  Scenario: Clinician Flow PRODSUPPQA
    Given user is on clinician_prodsuppqa Login Page.
    When the user provide the clinician_prodsuppqa username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in PRODSUPPQA environment.
    Then the user Register and books an appointment using the clinician in PRODSUPPQA.

  @Inventory_Supply_SIT
  Scenario: Inventory Supply Flow SIT
    Given user logged in as pphis_sit.
    When the user provides the pphis_sit username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user transfer doses from one clinic to another clinic in SIT environment
    Then the user transfer doses within the same clinic in SIT environment
    Then the user does the container adjustment in SIT environment
    Then the user does the container wastage in SIT environment
    Then the user does transfer and saves as draft and then transfer in SIT environment
    Then close the browser for pphis.

  @Inventory_Supply_UAT
  Scenario: Inventory Supply Flow UAT
    Given user logged in as pphis_uat.
    When the user provides the pphis_uat username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user transfer doses from one clinic to another clinic in UAT environment
    Then the user transfer doses within the same clinic in UAT environment
    Then the user does the container adjustment in UAT environment
    Then the user does the container wastage in UAT environment
    Then the user does transfer and saves as draft and then transfer in UAT environment
    Then close the browser for pphis.

  @Inventory_Supply_PRODSUPPQA
  Scenario: Inventory Supply Flow PRODSUPPQA
    Given user logged in as pphis_prodsuppqa.
    When the user provides the pphis_prodsuppqa username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user transfer doses from one clinic to another clinic in PRODSUPPQA environment
    Then the user transfer doses within the same clinic in PRODSUPPQA environment
    Then the user does the container adjustment in PRODSUPPQA environment
    Then the user does the container wastage in PRODSUPPQA environment
    Then the user does transfer and saves as draft and then transfer in PRODSUPPQA environment
    Then close the browser for pphis.

  @Inventory_Supply_SIT_Failed
  Scenario: Inventory Supply Flow SIT Failed
    Given user logged in as pphis_sit.
    When the user provides the pphis_sit username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user transfer doses from one clinic to another clinic in SIT environment for failed scenario
    Then close the browser for pphis.

  @Inventory_Supply_Bulk
  Scenario: Inventory Supply Flow Bulk.
    Given user is on pphis Login Page.
    When the user provides the pphis2 username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    #Then the user transfer doses from one clinic to another clinic
    #Then the user transfer doses from one clinic to another clinic for bulk transfers purpose
    #Then the user transfer doses within the same clinic
    Then the user does the container adjustment for bulk adjustment
    #Then the user does the container wastage for bulk wastage
    #Then the user does transfer and saves as draft and then transfer
    Then close the browser for pphis.